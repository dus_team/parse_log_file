import pandas as pd
import pymysql as pms
import time
source = 'LD4'

def parse_log_file_order(log_name):
    log_file = pd.read_table(log_name,header = None)
    log_file.columns = ['comment']
    log_file = log_file['comment']

    message_1 = [log.split('\x01') for log in log_file if len(log.split('\x01'))>2 and log.split('\x01')[2] == '35=U1']
    message_2 = [log.split('\x01') for log in log_file if len(log.split('\x01'))>2 and log.split('\x01')[2] == '35=U2']

    message_1 = [mes[:-1] if mes[-1] == '' else mes for mes in message_1]
    message_2 = [mes[:-1] if mes[-1] == '' else mes for mes in message_2]

    res_1 = {}
    res_2 = {}
    res = []
    for mes in message_1:
        res_1[mes[-2][3:11]] = mes[-2][12:]

    for mes in message_2:
        res_2[mes[-2][3:11]] = mes[-2][12:]

    for key in res_1.keys():
        res.append(key + ',' + res_1[key] + ',' + res_2[key] + ',' + source)

    print(res[0])


    print(message_1[0])
    print(len(message_1))
    print(message_2[0])
    print(len(message_2))

    with open(source + '_order.txt', 'w') as output:
        for line in res:
            output.write(line + '\n')

def insert_database_order(log):
    parse_log_file_order(log)

    with open('UKDB_SQL.cfg','r') as infile:
        cred = infile.read().splitlines()
    print(cred)

    print(cred[3][9:])
    print(cred[2][9:])
    print(cred[0][5:])
    print(int(cred[1][5:]))

    db = pms.connect(host=str(cred[0][5:]), port=int(cred[1][5:]),user= str(cred[2][9:-1]),password = str(cred[3][9:]),local_infile = 1)
    # db = pms.connect(host='ukdb.divisacapital.com', port=3306, user='becks', password='qSH@hnNZT%D3mdRr9$',local_infile = 1)
    cursor = db.cursor()

    # cursor.execute("""LOAD DATA LOCAL INFILE {0} REPLACE INTO TABLE {1} FIELDS TERMINATED BY ',' \
    #                LINES TERMINATED BY '\r\n'""".format('LD4_deal.txt','becks.test'))
    sql = """LOAD DATA LOCAL INFILE 'E:/OneDrive/Divisa Capital/parse_log_file/LD4_order.txt'
            REPLACE INTO TABLE dropcopy.order
            FIELDS TERMINATED BY ','
            LINES TERMINATED BY '\r\n'
          """
    cursor.execute(sql)
    db.commit()

    # print(lines)

    # db = connect
# start = time.time()
# parse_log_file_order('20170729_giveup.log')
# end = time.time()
# print(end - start)
# insert_database_order()
# insert_database_order('20170729_giveup.log')
