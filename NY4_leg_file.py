import pandas as pd
import pymysql as pms
source = 'NY4'

def parse_log_file_leg(log_name):
    log_file = pd.read_table(log_name,header = None)
    log_file.columns = ['comment']
    log_file = log_file['comment']

    message_leg_open = [log.split('\x01') for log in log_file if len(log.split('\x01'))>2 and log.split('\x01')[2] == '35=U3']
    message_leg_close = [log.split('\x01') for log in log_file if len(log.split('\x01'))>2 and log.split('\x01')[2] == '35=U4']

    message_leg_open = [mes[:-1] if mes[-1] == '' else mes for mes in message_leg_open]
    message_leg_close = [mes[:-1] if mes[-1] == '' else mes for mes in message_leg_close]

    print(message_leg_open)
    print(len(message_leg_open))
    print(message_leg_close)
    print(len(message_leg_close))

    res_open = {}
    for mes in message_leg_open:
        res_open[mes[-2][3:13]] = mes[-2][14:]

    res_close = {}
    for mes in message_leg_close:
        res_close[mes[-2][3:13]] = mes[-2][14:]

    res = []
    for key in res_open.keys():
        res.append(key + ',' + res_open[key] + ',' + res_close[key] + ',' + source)
    # print(res)
    # print([log for log in res if log[:8]== '16133621'])
    # count = 0
    with open(source + '_leg.txt','w') as output:
        for line in res:
            output.write(line + '\n')
            # count += 1
    # print(count)
    # print(message[0].split('')[2])
    # order_status = [mes.split('')[2] for mes in message]



def insert_database_leg(log):
    parse_log_file_leg(log)

    with open('UKDB_SQL.cfg','r') as infile:
        cred = infile.read().splitlines()
    print(cred)

    print(cred[3][9:])
    print(cred[2][9:])
    print(cred[0][5:])
    print(int(cred[1][5:]))

    db = pms.connect(host = str(cred[0][5:]),port = int(cred[1][5:]),user= str(cred[2][9:-1]),password = str(cred[3][9:]),local_infile = 1)
    # db = pms.connect(host='ukdb.divisacapital.com', port=3306, user='becks', password='qSH@hnNZT%D3mdRr9$',local_infile = 1)
    cursor = db.cursor()

    # cursor.execute("""LOAD DATA LOCAL INFILE {0} REPLACE INTO TABLE {1} FIELDS TERMINATED BY ',' \
    #                LINES TERMINATED BY '\r\n'""".format('LD4_deal.txt','becks.test'))
    sql = """LOAD DATA LOCAL INFILE 'E:/OneDrive/Divisa Capital/parse_log_file/NY4_leg.txt'
            REPLACE INTO TABLE dropcopy.leg
            FIELDS TERMINATED BY ','
            LINES TERMINATED BY '\r\n'
          """
    cursor.execute(sql)
    db.commit()

    # print(lines)
parse_log_file_leg('20170729_giveup_US.log')
#     # db = connect
# insert_database_leg()
