import pandas as pd
import pymysql as pms
import collections

file = pd.read_table('20170729_divisa.log',header = None)
file = file[0]

message = []
for mes in file:
    message.append(mes.split(': '))

order_open_list = {}
order_close_list = {}
deal_done_list = {}
leg_open_list = {}
leg_close_list = {}
order_list = {}
leg_list = {}

print(file[0])
print(message)
for mes in message:
    if 'onOrderOpen' in mes[0]:
        order_open_list[mes[1].strip()] = mes[2]
    if 'onOrderClose' in mes[0]:
        order_close_list[mes[1].strip()] = ','.join(mes[2].split(',')[1:])
    if 'onDealDone' in mes[0]:
        deal_done_list[mes[1].strip()] = mes[2] + ',LD4'
    if 'onLegOpen' in mes[0]:
        leg_open_list[mes[1].strip()] = mes[2]
    if 'onLegClose' in mes[0]:
        leg_close_list[mes[1].strip()] = ','.join(mes[2].split(',')[2:])

filtered_deal_list = [mes for mes in deal_done_list.values() if len(mes.split(',')) == 12]

for key in order_open_list.keys():
    order_list[key] = order_open_list[key] + ',' + order_close_list[key] + ',LD4'

for key in leg_open_list.keys():
    leg_list[key] = leg_open_list[key] + ',' + leg_close_list[key] + ',LD4'

# length = [len(mes.split(',')) for mes in leg_list.values()]
# print(collections.Counter(length))

filtered_order_list = [mes for mes in order_list.values() if len(mes.split(',')) == 36]

with open('new_order.txt','w') as output_order:
    for value in filtered_order_list:
        output_order.write(value + '\n')

with open('new_leg.txt','w') as output_leg:
    for value in leg_list.values():
        output_leg.write(value + '\n')

# length = []
# for value in deal_done_list.values():
#     length.append(len(value.split(',')))
#     if len(value.split(',')) == 10:
#         print(value)
# print(collections.Counter(filtered_deal_list))

# print(deal_done_list)
with open('new_deal.txt','w') as output_deal:
    for value in filtered_deal_list:
        output_deal.write(value +  '\n' )

def insert_deal():
    db = pms.connect(host='ukdb.divisacapital.com', port=3306, user='becks', password='qSH@hnNZT%D3mdRr9$',local_infile = 1)
    cursor = db.cursor()

    sql = """LOAD DATA LOCAL INFILE 'E:/OneDrive/Divisa Capital/parse_log_file/new_deal.txt'
                REPLACE INTO TABLE dropcopy.deal
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\r\n'
              """
    cursor.execute(sql)
    db.commit()

def insert_order():
    db = pms.connect(host='ukdb.divisacapital.com', port=3306, user='becks', password='qSH@hnNZT%D3mdRr9$',local_infile = 1)
    cursor = db.cursor()

    sql = """LOAD DATA LOCAL INFILE 'E:/OneDrive/Divisa Capital/parse_log_file/new_order.txt'
                REPLACE INTO TABLE dropcopy.order
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\r\n'
              """
    cursor.execute(sql)
    db.commit()

def insert_leg():
    db = pms.connect(host='ukdb.divisacapital.com', port=3306, user='becks', password='qSH@hnNZT%D3mdRr9$',
                     local_infile=1)
    cursor = db.cursor()

    sql = """LOAD DATA LOCAL INFILE 'E:/OneDrive/Divisa Capital/parse_log_file/new_leg.txt'
                REPLACE INTO TABLE dropcopy.leg
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\r\n'
              """
    cursor.execute(sql)
    db.commit()

insert_leg()
insert_order()
insert_deal()